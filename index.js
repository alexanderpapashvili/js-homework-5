let subjectsName = ["javascript", "react", "python", "java"];

// subjects
let subjectCredits = {
  javascript: 4,
  react: 7,
  python: 6,
  java: 3,
};
subjectCredits.creditSum = 0;
for (let i = 0; i < subjectsName.length; i++) {
  subjectCredits.creditSum += subjectCredits[subjectsName[i]];
}

// students
let students = [];

// student 1
students[0] = {
  name: "Jean",
  surname: "Reno",
  age: 26,
  scores: {
    javascript: 62,
    react: 57,
    python: 88,
    java: 90,
  },
};

// student 2
students[1] = {
  name: "Claude",
  surname: "Monet",
  age: 19,
  scores: {
    javascript: 77,
    react: 52,
    python: 92,
    java: 67,
  },
};

// student 3
students[2] = {
  name: "Van",
  surname: "Gogh",
  age: 21,
  scores: {
    javascript: 51,
    react: 98,
    python: 65,
    java: 70,
  },
};

// student 4
students[3] = {
  name: "Dam",
  surname: "Square",
  age: 36,
  scores: {
    javascript: 82,
    react: 53,
    python: 80,
    java: 65,
  },
};

// students' sum
for (let i = 0; i < students.length; i++) {
  students[i].scores.sum = 0;
}
for (let i = 0; i < subjectsName.length; i++) {
  for (let j = 0; j < students.length; j++) {
    students[j].scores.sum += students[j].scores[subjectsName[i]];
  }
}

//arithmetical average
for (let i = 0; i < students.length; i++) {
  students[i].scores.arithmeticalAverage = students[i].scores.sum / 4;
}

//GPA
students[0].scores.coef = [1, 0.5, 3, 3];
students[1].scores.coef = [2, 0.5, 4, 1];
students[2].scores.coef = [0.5, 4, 1, 1];
students[3].scores.coef = [3, 0.5, 2, 1];
for (let i = 0; i < students.length; i++) {
  students[i].scores.GPA = 0;
  for (let j = 0; j < subjectsName.length; j++) {
    students[i].scores.GPA +=
      students[i].scores.coef[j] * subjectCredits[subjectsName[j]];
  }
  students[i].scores.GPA /= subjectCredits.creditSum;
}

// arithmetical averages
let generalArthAve = 0;
for (let i = 0; i < students.length; i++) {
  generalArthAve += students[i].scores.arithmeticalAverage;
}
generalArthAve /= 4;
for (let i = 0; i < students.length; i++) {
  students[i].scores.arithmeticalAverage > generalArthAve
    ? console.log(
        `${students[i].name} ${students[i].surname} got the Red Diploma!`
      )
    : console.log(
        `${students[i].name} ${students[i].surname} is in "ვრაგ ნაროდ"!`
      );
}

// the best student with GPA
console.log("-------------------------------------------------------");
let bestGPAName = students[0].name;
let bestGPASurname = students[0].name;
let maxGPA = students[0].scores.GPA;
for (let i = 0; i < students.length; i++) {
  if (students[i].scores.GPA > maxGPA) {
    maxGPA = students[i].scores.GPA;
    bestGPAName = students[i].name;
    bestGPASurname = students[i].surname;
  }
}
console.log(`${bestGPAName} ${bestGPASurname} has the highest GPA!`);

// the best student with the best arithmetical average (21+)
console.log("-------------------------------------------------------");
let bestArthAveOver21Name = "";
let bestArthAveOver21Surname = "";
let maxValue = 0;
for (let i = 0; i < students.length; i++) {
  if (
    students[i].age >= 21 &&
    students[i].scores.arithmeticalAverage > maxValue
  ) {
    maxValue = students[i].scores.arithmeticalAverage;
    bestArthAveOver21Name = students[i].name;
    bestArthAveOver21Surname = students[i].surname;
  }
}
console.log(
  `${bestArthAveOver21Name} ${bestArthAveOver21Surname} has best arithmetical average over 21!`
);

// the best front-end developer
console.log("-------------------------------------------------------");
for (let i = 0; i < students.length; i++) {
  students[i].scores.frontEnd = 0;
  students[i].scores.frontEnd =
    students[i].scores.javascript + students[i].scores.react;
}
let bestFrontEndName = students[0].name;
let bestFrontEndSurname = students[0].name;
let maxFrontEndScore = students[0].scores.frontEnd;

for (let i = 0; i < students.length; i++) {
  if (students[i].scores.frontEnd > maxFrontEndScore) {
    maxFrontEndScore = students[i].scores.frontEnd;
    bestFrontEndName = students[i].name;
    bestFrontEndSurname = students[i].surname;
  }
}
console.log(
  `${bestFrontEndName} ${bestFrontEndSurname} is the best front-end developer!`
);
