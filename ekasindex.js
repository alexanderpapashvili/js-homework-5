// tourists
let tourists = [];

// tourist 1
tourists[0] = {
  name: "Mark",
  age: 19,
  visited: ["Tbilisi", "London", "Rome", "Berlin"],
  amount: [120, 200, 150, 140],
};
// tourist 2
tourists[1] = {
  name: "Bob",
  age: 21,
  visited: ["Miami", "Moscow", "Vena", "Riga", "Kyiv"],
  amount: [90, 240, 100, 76, 123],
};
// tourist 3
tourists[2] = {
  name: "Sam",
  age: 22,
  visited: ["Tbilisi", "Budapest", "Warsaw", "Vilnius"],
  amount: [118, 95, 210, 236],
};
// tourist 4
tourists[3] = {
  name: "Anna",
  age: 20,
  visited: ["New-York", "Athens", "Sidney", "Tokyo"],
  amount: [100, 240, 50, 190],
};
// tourist 5
tourists[4] = {
  name: "Alex",
  age: 23,
  visited: ["Paris", "Tbilisi", "Madrid", "Marcel", "Minsk"],
  amount: [96, 134, 76, 210, 158],
};

//adult
for (let i = 0; i < tourists.length; i++) {
  tourists[i].adult = tourists[i].age > 21;
}

// visisted Georgia
for (let i = 0; i < tourists.length; i++) {
  tourists[i].visitedGeorgia = "Not visited";
  for (let j = 0; j < tourists[i].visited.length; j++) {
    if (tourists[i].visited[j] == "Tbilisi") {
      tourists[i].visitedGeorgia = "Visited";
    }
  }
}

// money
for (let i = 0; i < tourists.length; i++) {
  let x = 0;
  for (let j = 0; j < tourists[i].amount.length; j++) {
    x += tourists[i].amount[j];
  }
  tourists[i].amount["Total"] = x;
}

// money average
let sumMoney = 0;
for (let i = 0; i < tourists.length; i++) {
  sumMoney += tourists[i].amount["Total"];
}
sumMoney /= tourists.length;
console.log(`Average Money Spent: ${sumMoney}.`);

// rich tourist
let richAmount = tourists[0].amount["Total"];
let richTouristName = "";
for (let i = 0; i < tourists.length; i++) {
  if (tourists[i].amount["Total"] > richAmount) {
    richAmount = tourists[i].amount["Total"];
    richTouristName = tourists[i].name;
  }
}
console.log(`The richest tourist is ${richTouristName}. Spent: ${richAmount}`);

// tourists
console.log(tourists);
